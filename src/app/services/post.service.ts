import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http:HttpClient) { }

  getPost() {
    //return ['Superman', 'Batman', 'Ultraman', 'Spiderman'];
    return [
      {id:1, title:'Test 1'},
      {id:2, title:'Test 2'},
      {id:3, title:'Test 3'}
    ];
  }

  // read data from jsonplaceholder.typicode.com
  getPost2() {
    let url = 'https://jsonplaceholder.typicode.com/posts';
    return this.http.get(url);
  }

  getDetail(id:number) : Observable<any> {
    let url = 'https://jsonplaceholder.typicode.com/posts/' + id;
    return this.http.get(url);
  }
}
