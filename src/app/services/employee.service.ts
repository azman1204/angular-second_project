import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  employees: any = [
    {id:1, name: 'Azman Zakaria', email:'azman1204@yahoo.com'},
    {id:2, name: 'John Doe', email:'john@yahoo.com'},
    {id:3, name: 'Abu Bakar Ellah', email:'abu@yahoo.com'},
  ];

  constructor() {}

  findAll() {
    return this.employees;
  }

  findOne(id:number) {
    return this.employees.find((employee: any) => {
      return employee.id == id;
    });
  }

  save(emp: any) {
    this.employees.push(emp);
  }

  edit(id:number) {
    return this.findOne(id);
  }

  update(id: number, emp:any) {
    this.employees = this.employees.filter((employee:any) => {
      if (employee.id == id) {
        employee.name = emp.name;
        employee.email = emp.email;
      }
      return employee;
    });
  }

  delete(id:number) {
    this.employees = this.employees.filter((employee:any) => {
      if (!(employee.id == id)) {
        return employee;
      }
    });
  }
}
