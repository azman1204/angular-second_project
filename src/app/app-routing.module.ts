import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { PostComponent } from './post/post.component';
import { EmployeeComponent } from './employee/employee.component';

const routes: Routes = [
  {path:'', component:HomeComponent}, // ini default route
  {path:'contact', component:ContactComponent}, // url localhost:4200/contact
  {path:'post', component:PostComponent},
  {path:'employee', component:EmployeeComponent},
  {path:'**', component:HomeComponent}, // ini utk kes route not found
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
