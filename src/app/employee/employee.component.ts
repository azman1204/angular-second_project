import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  name = '';
  email = '';
  employees: any = [];
  isShow = false; // show form

  constructor(private employee: EmployeeService) { }

  ngOnInit(): void {
    this.employees = this.employee.findAll();
  }

  save() {
    this.isShow = false;
    let id = Math.random() * 10000;
    let emp = {id: id, name: this.name, email:this.email};
    this.employee.save(emp);
    this.employee = this.employee.findAll();
  }
}
