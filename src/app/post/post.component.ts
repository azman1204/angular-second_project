import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  title = 'second_app';
  posts:any = [];
  post_ori:any = [];
  detail: any = {};
  // dependency injection
  constructor(private post: PostService) {}

  ngOnInit(): void {
    // ini return Observable - handle async data
    this.post.getPost2().subscribe((data) => {
      this.posts = data;
      this.post_ori = data;
      console.log(this.posts);
    });
  }

  doSearch(event:any) {
    // 13 = enter
    console.log(event);
    this.posts = this.post_ori;
    this.posts = this.posts.filter((post:any) => {
      return post.title.includes(this.title);
    });
    // if (event.key == 'Enter') {
    //   this.posts = this.posts.filter((post:any) => {
    //     return post.title.includes(this.title);
    //   });
    // } else {
    //   if (this.title === '') {
    //     this.posts = this.post_ori;
    //   }
    // }
    //alert('you are searching..');
  }

  doDetail(id:number) {
    this.post.getDetail(id).subscribe((data) => {
      this.detail = data;
    });
  }

}
